# Plant Disease Detection 

Welcome to the Plant Disease Detection project! This project focuses on the detection of plant diseases using the EfficientNetB0 model. Two variations of the model were trained: one using the original dataset and the other using an augmented dataset. The goal is to compare the performance of the model trained with and without augmented images.

## Overview

The project utilizes the EfficientNetB0 architecture, a powerful convolutional neural network, for plant disease detection tasks. The model is trained on a dataset comprising images of diseased and healthy plant leaves. By leveraging transfer learning techniques, the model learns to accurately classify plant diseases and provides valuable insights for early disease detection and crop management.

## Key Features

- **EfficientNetB0 Model**: The model architecture chosen for its balance of accuracy and computational efficiency, suitable for deployment in resource-constrained environments.
- **Original Dataset**: The model is initially trained on the original dataset without any data augmentation techniques applied.
- **Augmented Dataset**: To improve model generalization and robustness, an augmented dataset is created by applying various augmentation techniques such as rotation, flipping, and scaling to the original images.
- **Performance Comparison**: The performance of the model trained with the original and augmented datasets is compared to evaluate the impact of data augmentation on detection accuracy and generalization capability.

## Model Performance

The performance of the model trained with both the original and augmented datasets is summarized below:

| Model Variation                               | Accuracy   |
|-----------------------------------------------|------------|
| EfficientNetB0 trained on Original Dataset    | 97.1%      |
| EfficientNetB0 trained on Augmented Dataset   | 97.3%      |

## Example Images

### Model Performance on Original Dataset

![Model Performance](history_plot1.png)
![Confusion Matrix](confusion_matrix1.png)


### Model Performance on Augmented Dataset 
![Model Performance](history_plot2.png)
![Confusion Matrix](confusion_matrix2.png)

The above images showcase the performance of the model trained with the original and augmented datasets, respectively. The visualization includes sample input images and their corresponding predictions.

## Conclusion

The Plant Disease Detection project demonstrates the effectiveness of leveraging transfer learning techniques with the EfficientNetB0 model for accurate disease detection in plants. By comparing the performance of models trained with original and augmented datasets, it is evident that data augmentation enhances model accuracy and generalization capability, leading to improved disease detection results.
